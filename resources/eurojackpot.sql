CREATE TABLE "winning_numbers" (
  "id" SERIAL PRIMARY KEY,
  "five_of_fifty" integer[5],
  "two_of_ten" integer[2],
  "date" date
);

CREATE TABLE "logs" (
  "id" SERIAL PRIMARY KEY,
  "updated_at" date
);

CREATE TABLE "combinations" (
  "id" SERIAL PRIMARY KEY,
  "combination_of" int,
  "combination" integer[],
  "count" int
);

CREATE TABLE "generated_combinations" (
  "combination" int[],
  "combinations_id" int,
  "sum" int,
  PRIMARY KEY ("combination", "combinations_id")
);

ALTER TABLE "generated_combinations" ADD FOREIGN KEY ("combinations_id") REFERENCES "combinations" ("id");

