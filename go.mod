module gitlab.com/JustUrDentist/GoEurojackpot

go 1.14

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/jackc/pgx/v4 v4.9.0
	github.com/pkg/errors v0.9.1
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a // indirect
)
