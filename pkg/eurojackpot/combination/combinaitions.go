package combination

import (
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/helpers"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/math"
	"strconv"
)

type Combination struct {
	ID            int
	CombinationOf int
	Numbers       []int
	Count         int
}

func (c *Combination) ToSQLString() string {
	return "('" + strconv.Itoa(c.CombinationOf) + "', " + helpers.IntArrayToSQLString(c.Numbers) + ", '" + strconv.Itoa(c.Count) + "')"
}

func Remove(combinations []Combination, i int) []Combination {
	lenComb := len(combinations)
	combinations[i] = combinations[lenComb-1]
	return combinations[:lenComb-1]
}

type Combinations struct {
	Combination []Combination
	MaxCount    int
	MinCount    int
	generate    *Generate
}

func (cc *Combinations) minMaxCount() {
	cc.MinCount = 9999
	cc.MaxCount = 0
	for _, c := range cc.Combination {
		if c.Count < cc.MinCount {
			cc.MinCount = c.Count
		}
		if c.Count > cc.MaxCount {
			cc.MaxCount = c.Count
		}
	}
}

func (cc *Combinations) createCombinations(amountComb int) {
	var allComb [][]int
	cc.Combination = []Combination{}

	var comb Combination
	comb.Count = 0

	for _, number := range cc.generate.DNs.Numbers {
		math.Permutations(number.FiveOfFifty[:], 0, 5, 0, amountComb, &allComb)

		for j := range allComb {
			if cc.existCombination(&allComb[j]) {
				cc.Combination[cc.findCombination(&allComb[j])].Count++
			} else {
				comb.CombinationOf = amountComb
				comb.Numbers = allComb[j]
				comb.Count = 1
				cc.Combination = append(cc.Combination, comb)
				comb = Combination{}
				comb.Count = 0
			}
		}
		allComb = [][]int{}
	}
	cc.minMaxCount()
}

func (cc *Combinations) safeCombinations(comb int) {
	cc.createCombinations(comb)
	InsertCombinationsOf(&cc.Combination)
}

func (cc *Combinations) combination(comb int) {
	cc.Combination = SelectCombinations(comb)
	cc.minMaxCount()
}

func (cc *Combinations) existCombination(combination *[]int) bool {
	lenArr := len(cc.Combination)

	if lenArr == 0 {
		return false
	}

	for i := 0; i < lenArr; i++ {
		if helpers.CompareIntArrays(&cc.Combination[i].Numbers, combination) {
			return true
		}
	}

	return false
}

func (cc *Combinations) findCombination(arr *[]int) int {
	for key, combination := range cc.Combination {
		numbers := combination.Numbers[:]
		if helpers.CompareIntArrays(&numbers, arr) {
			return key
		}
	}
	return 0
}
