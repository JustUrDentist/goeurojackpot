package combination

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/helpers"
)

var (
	insertGeneratedCombinationsOff      = "INSERT INTO generated_combinations (combination, combinations_id, sum) VALUES %s"
	selectGeneratedCombinationsf        = "SELECT g.combination FROM generated_combinations g JOIN combinations c ON g.combinations_id = c.id WHERE c.combination_of = %d AND c.count = %d AND g.sum BETWEEN %d AND %d OFFSET %d LIMIT %d"
	selectGeneratedCombinationsNoLimitf = "SELECT g.combination FROM generated_combinations g JOIN combinations c ON g.combinations_id = c.id WHERE c.combination_of = %d AND c.count = %d AND g.sum BETWEEN %d AND %d"
	truncate                            = "TRUNCATE TABLE combinations, generated_combinations RESTART IDENTITY CASCADE"
)

func Truncate() {
	db := helpers.DatabaseHelper{}.New()

	db.Inserter(truncate)
}

func InsertGeneratedCombinations(values string) {
	db := helpers.DatabaseHelper{}.New()

	db.Inserter(db.Queryf(insertGeneratedCombinationsOff, values))
}

func SelectGeneratedCombinations(of int, count int, sumFrom uint, sumTo uint, limit uint, page uint) [][]int {
	db := helpers.DatabaseHelper{}.New()
	var conn *pgxpool.Conn
	var rows pgx.Rows

	var result [][]int
	var comb []int

	query := db.Queryf(selectGeneratedCombinationsf, of, count, sumFrom, sumTo, page*limit, limit)

	conn = db.AcquireConn()

	rows, db.Err = conn.Query(context.TODO(), query)
	db.Catch()

	for rows.Next() {
		db.Err = rows.Scan(&comb)
		db.Catch()

		result = append(result, comb)
	}

	conn.Release()

	return result
}

func SelectGeneratedCombinationsNoLimit(of int, count int, sumFrom uint, sumTo uint) [][]int {
	db := helpers.DatabaseHelper{}.New()
	var conn *pgxpool.Conn
	var rows pgx.Rows

	var result [][]int
	var comb []int

	query := db.Queryf(selectGeneratedCombinationsNoLimitf, of, count, sumFrom, sumTo)

	conn = db.AcquireConn()

	rows, db.Err = conn.Query(context.TODO(), query)
	db.Catch()

	for rows.Next() {
		db.Err = rows.Scan(&comb)
		db.Catch()

		result = append(result, comb)
	}

	conn.Release()

	return result
}
