package combination

import (
	"testing"
)

func TestMinMaxCount(t *testing.T) {
	c1 := Combination{
		NumberCombination: "1 2",
		Numbers:           []int{1, 2},
		Count:             1,
	}

	c2 := Combination{
		NumberCombination: "1 ",
		Numbers:           []int{1, 3},
		Count:             3,
	}

	cc := Combinations{
		Combination: nil,
		MaxCount:    0,
		MinCount:    0,
		generate:    nil,
	}

	cc.Combination = append(cc.Combination, c1)
	cc.Combination = append(cc.Combination, c2)

	cc.minMaxCount()

	if cc.MinCount != 1 || cc.MaxCount != 3 {
		t.Error("minMaxCount was incorrect. expected: min/max ", 1, "/", 3, " get: min/max ", cc.MinCount, "/", cc.MaxCount)
	}

}

func TestExistCombination(t *testing.T) {
	comb := Combination{
		NumberCombination: "2 3 4",
		Numbers:           []int{2, 3, 4},
		Count:             2,
	}

	var combs Combinations
	combs.Combination = append(combs.Combination, comb)

	testComb := []int{2, 3, 4}

	if !combs.existCombination(&testComb) {
		t.Error("existCombinations was incorrect. expected: ", true, " get: ", false)
	}
}

func TestExistAnyNumberInCombination(t *testing.T) {
	numbers := [5]int{1, 2, 3, 4, 5}
	combinations := [5]int{6, 7, 8, 9, 5}

	if !existAnyNumberInCombination(&numbers, &combinations) {
		t.Error("existAnyNumberInCombination was incorrect. expected: ", true, " get: ", false)
	}
}
