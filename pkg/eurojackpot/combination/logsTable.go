package combination

import (
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/helpers"
	"time"
)

var (
	selectLatest = "SELECT MAX(updated_at) AS result FROM logs"
	insertLogf   = "INSERT INTO logs (updated_at) VALUES ('%s')"
)

func LatestUpdate() time.Time {
	db := helpers.DatabaseHelper{}.New()
	var result time.Time

	db.SingleSelect(selectLatest, &result)

	return result
}

func Updated() {
	db := helpers.DatabaseHelper{}.New()

	db.Inserter(db.Queryf(insertLogf, time.Now().Format("2006-1-2")))
}
