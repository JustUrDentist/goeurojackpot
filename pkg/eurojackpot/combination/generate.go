package combination

import (
	"fmt"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/drawnNumber"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/helpers"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/math"
	"math/rand"
	"sort"
	"sync"
	"time"
)

var (
	once sync.Once
)

type Generate struct {
	CombOf       int
	CombCount    int
	SumFrom      uint
	SumTo        uint
	Limit        uint
	DNs          drawnNumber.DrawnNumbers
	Combinations Combinations
	RandomUnique [][]int
}

func (g *Generate) init() {
	once.Do(func() {
		g.DNs.Numbers = drawnNumber.SelectDNs()
		g.Combinations.generate = g
	})
}

func (g *Generate) Update() {
	g.init()
	for amountComb := 2; amountComb <= 4; amountComb++ {
		g.Combinations.createCombinations(amountComb)
		g.Combinations.Combination = UpdateCombinationsOf(&g.Combinations.Combination)

		if len(g.Combinations.Combination) <= 0 {
			continue
		}
		g.Combinations.minMaxCount()

		wg := sync.WaitGroup{}
		wg.Add(g.Combinations.MaxCount - g.Combinations.MinCount + 1)
		for common := g.Combinations.MinCount; common <= g.Combinations.MaxCount; common++ {
			if !ExistCombinationCount(amountComb, common) {
				wg.Done()
				continue
			}

			go func(comm int) {
				defer wg.Done()
				g.safeNumberCombinations(amountComb, comm)
			}(common)

		}
		wg.Wait()
	}
	Updated()
	fmt.Println("All files Updated")
	return
}

func (g *Generate) AllNew() {
	g.init()
	Truncate()
	for amountComb := 2; amountComb <= 4; amountComb++ {

		g.Combinations.safeCombinations(amountComb)
		wg := sync.WaitGroup{}
		wg.Add(g.Combinations.MaxCount - g.Combinations.MinCount + 1)
		for common := g.Combinations.MinCount; common <= g.Combinations.MaxCount; common++ {
			if !ExistCombinationCount(amountComb, common) {
				wg.Done()
				continue
			}

			go func(common int) {
				defer wg.Done()
				g.safeNumberCombinations(amountComb, common)
			}(common)

		}
		wg.Wait()
	}
	fmt.Println("All files new created")
	return
}

func (g *Generate) safeNumberCombinations(amountComb int, common int) {
	wg := sync.WaitGroup{}

	lenCC := len(g.Combinations.Combination)
	part := math.NextGreaterDivisor(lenCC, 10)

	partLen := lenCC / part
	wg.Add(part)

	for i := 0; i < part; i++ {
		go func(start int, end int) {
			defer wg.Done()
			fiveNumbersCombinationThread(&g.DNs, g.Combinations, amountComb, common, start, end)
		}(i*partLen, (i+1)*partLen)
	}

	wg.Wait()
	fmt.Printf("comb Of %d with count of %d is saved\n", amountComb, common)

	return
}

func fiveNumbersCombinationThread(dns *drawnNumber.DrawnNumbers, cc Combinations, combOf int, combCount int, start int, end int) {
	for i := start; i < end; i++ {
		if cc.Combination[i].Count != combCount {
			continue
		}

		numPool := generateNumberPool()

		var numPoolCombs [][]int
		for _, c := range cc.Combination[i].Numbers {
			delete(numPool, c)
		}
		numPoolArr := helpers.MapToIntArray(numPool)

		sort.Ints(numPoolArr)
		math.Permutations(numPoolArr, 0, len(numPoolArr), 0, 5-combOf, &numPoolCombs)

		generateCombination(&numPoolCombs, dns, cc, i)
	}
}

func generateCombination(numPoolCombs *[][]int, dns *drawnNumber.DrawnNumbers, cc Combinations, index int) {
	var numbers []int
	var values string
	id := SelectCombinationsId(cc.Combination[index].Numbers)

	i := 0
	for _, numPoolComb := range *numPoolCombs {

		numbers = cc.Combination[index].Numbers
		setNewNum := true
		for j := 0; j < len(numPoolComb); j++ {
			numbers = append(numbers, numPoolComb[j])
		}

		sort.Ints(numbers)

		for _, n := range dns.Numbers {
			fiveOfFifty := n.FiveOfFifty[:]
			if helpers.CompareIntArrays(&fiveOfFifty, &numbers) {
				setNewNum = false
			}
		}

		if setNewNum {
			values += fmt.Sprintf("(%s, '%d', '%d'), ", helpers.IntArrayToSQLString(numbers), id, helpers.SumIntArray(numbers))
			i++
			if i >= 1000 {
				i = 0
				helpers.RemoveLastPattern(", ", &values)
				InsertGeneratedCombinations(values)
				values = ""
			}
		}
		numbers = []int{}
	}
	if i > 0 && i < 1000 {
		helpers.RemoveLastPattern(", ", &values)
		InsertGeneratedCombinations(values)
	}
}

func (g *Generate) RandomUniqueCombinations() [][]int {
	rand.Seed(time.Now().UnixNano())

	combs := g.AllCombinationNumbers()

	g.RandomUnique = [][]int{}
	g.RandomUnique = append(g.RandomUnique, combs[rand.Intn(len(combs))])
	i := 1
	for _, c := range combs {
		combinationUniqueCount := 0
		for _, combinationResult := range g.RandomUnique {
			if existAnyNumberInCombination(&combinationResult, &c) {
				break
			} else {
				combinationUniqueCount++
			}
		}
		if combinationUniqueCount == len(g.RandomUnique) {
			g.RandomUnique = append(g.RandomUnique, c)
			i++
		}
	}

	return g.RandomUnique
}

func (g *Generate) AllCombinationNumbers() [][]int {
	return SelectGeneratedCombinationsNoLimit(g.CombOf, g.CombCount, g.SumFrom, g.SumTo)
}

func generateNumberPool() map[int]int {
	var pool = map[int]int{}
	for i := 1; i <= 50; i++ {
		pool[i] = i
	}
	return pool
}

func existNumberInCombination(number *int, combination *[]int) bool {
	for _, c := range *combination {
		if *number == c {
			return true
		}
	}

	return false
}

func existAnyNumberInCombination(numbers *[]int, combination *[]int) bool {
	for _, number := range *numbers {
		if existNumberInCombination(&number, combination) {
			return true
		}
	}

	return false
}

func UpdatedNeeded() bool {
	lastUpdate := LatestUpdate()
	lastDN := drawnNumber.SelectLastDN()

	return lastDN.After(lastUpdate)
}
