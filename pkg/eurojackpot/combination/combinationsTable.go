package combination

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/helpers"
	"strconv"
)

var (
	insertCombination          = "INSERT INTO combinations (combination_of, combination, count) VALUES "
	selectCombinations         = "SELECT * FROM combinations WHERE combination_of = "
	selectCombinationsByCountf = "SELECT count(*) FROM combinations WHERE combination_of = %d AND count = %d"
	selectCombinationsID       = "SELECT id FROM combinations WHERE combination = %s"
	updateCombinationf         = "UPDATE combinations SET count = %d WHERE combination = %s AND combination_of = %d"
)

func InsertCombinationsOf(combinations *[]Combination) {
	db := helpers.DatabaseHelper{}.New()

	if len(*combinations) <= 0 {
		return
	}

	query := insertCombination

	for _, c := range *combinations {
		query += c.ToSQLString() + ", "
	}

	err := helpers.RemoveLastPattern(", ", &query)
	if err != nil {
		panic(err)
	}

	db.Inserter(query)
}

func ExistCombinationCount(of int, count int) bool {
	db := helpers.DatabaseHelper{}.New()
	var rows int

	db.SingleSelect(db.Queryf(selectCombinationsByCountf, of, count), &rows)

	return rows > 0
}

func SelectCombinations(of int) []Combination {
	db := helpers.DatabaseHelper{}.New()
	var conn *pgxpool.Conn

	var rows pgx.Rows
	var id int
	var combOf int
	var comb []int
	var count int
	var result []Combination

	query := selectCombinations + strconv.Itoa(of)

	conn = db.AcquireConn()

	rows, db.Err = conn.Query(context.TODO(), query)
	db.Catch()

	for rows.Next() {
		db.Err = rows.Scan(&id, &combOf, &comb, &count)
		db.Catch()

		combination := Combination{
			ID:            id,
			CombinationOf: combOf,
			Numbers:       comb,
			Count:         count,
		}

		result = append(result, combination)
	}

	conn.Release()

	return result
}

func SelectCombinationsId(comb []int) int {
	db := helpers.DatabaseHelper{}.New()
	var id int
	db.SingleSelect(db.Queryf(selectCombinationsID, helpers.IntArrayToSQLString(comb)), &id)

	return id
}

func UpdateCombinationsOf(comb *[]Combination) []Combination {
	var oldComb []Combination
	var newComb []Combination

	combOf := (*comb)[0].CombinationOf

	oldComb = SelectCombinations(combOf)

	if len(*comb) > len(oldComb) {
		tmpOld := oldComb
		newComb = *comb

		removeEqualsByCombination(&tmpOld, &newComb)

		InsertCombinationsOf(&newComb)
	}

	tmpOld := oldComb
	newComb = *comb

	removeEquals(&tmpOld, &newComb)

	for _, c := range newComb {
		UpdateCombinationOf(c.Count, helpers.IntArrayToSQLString(c.Numbers), combOf)
	}

	return newComb
}

func UpdateCombinationOf(newCount int, combination string, of int) {
	db := helpers.DatabaseHelper{}.New()

	db.Updater(db.Queryf(updateCombinationf, newCount, combination, of))
}

func removeEqualsByCombination(oldComb *[]Combination, newComb *[]Combination) {
	keepGoing := true
	for keepGoing {
		keepGoing = func(oldComb *[]Combination, newComb *[]Combination) bool {
			for i, newC := range *newComb {
				for j, oldC := range *oldComb {
					newNumbers := newC.Numbers[:]
					oldNumbers := oldC.Numbers[:]
					if helpers.CompareIntArrays(&newNumbers, &oldNumbers) {
						*oldComb = Remove(*oldComb, j)
						*newComb = Remove(*newComb, i)

						return true
					}
				}
			}
			return false
		}(oldComb, newComb)
	}
}

func removeEquals(oldComb *[]Combination, newComb *[]Combination) {
	keepGoing := true
	for keepGoing {
		keepGoing = func(oldComb *[]Combination, newComb *[]Combination) bool {
			for i, newC := range *newComb {
				for j, oldC := range *oldComb {
					newNumbers := newC.Numbers[:]
					oldNumbers := oldC.Numbers[:]
					if helpers.CompareIntArrays(&newNumbers, &oldNumbers) && newC.Count == oldC.Count {
						*oldComb = Remove(*oldComb, j)
						*newComb = Remove(*newComb, i)

						return true
					}
				}
			}
			return false
		}(oldComb, newComb)
	}
}
