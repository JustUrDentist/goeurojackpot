package helpers

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"sync"
	"time"
)

const (
	DBUrl     = "postgresql://postgres:12345678@localhost:5432/postgres?pool_max_conns=30"
	TIMEOUTED = 120 * time.Second
)

type DatabaseHelper struct {
	Pool *pgxpool.Pool
	Err  error
}

var (
	singleton *DatabaseHelper
	once      sync.Once
)

func (db DatabaseHelper) New() *DatabaseHelper {
	once.Do(func() {
		singleton = &DatabaseHelper{}
	})

	return singleton
}

func (db *DatabaseHelper) AcquireConn() *pgxpool.Conn {
	var conn *pgxpool.Conn
	conn = nil

	timeOut := time.Now().Add(TIMEOUTED)

	for conn == nil || time.Now().After(timeOut) {
		conn, db.Err = db.Pool.Acquire(context.TODO())
	}

	return conn
}

func (db *DatabaseHelper) Inserter(sql string, values ...interface{}) {
	var conn *pgxpool.Conn

	conn = db.AcquireConn()
	_, db.Err = conn.Query(context.TODO(), sql, values...)
	db.Catch()

	conn.Release()
}

func (db *DatabaseHelper) Updater(sql string) {
	db.Inserter(sql)
}

func (db *DatabaseHelper) SingleSelect(sql string, output ...interface{}) {
	var conn *pgxpool.Conn

	conn = db.AcquireConn()

	db.Err = conn.QueryRow(context.TODO(), sql).Scan(output...)
	db.Catch()

	conn.Release()
}

func (db *DatabaseHelper) MultiSelect(sql string, columns int) {

}

func (db *DatabaseHelper) Query(v ...interface{}) string {
	return fmt.Sprint(v...)
}

func (db *DatabaseHelper) Queryf(sql string, v ...interface{}) string {
	return fmt.Sprintf(sql, v...)
}

func (db *DatabaseHelper) Catch() {
	if db.Err != nil {
		panic(db.Err)
	}
}

func (db *DatabaseHelper) Open() {
	db.Pool, db.Err = pgxpool.Connect(context.TODO(), DBUrl)
	db.Catch()
}

func (db *DatabaseHelper) Close() {
	db.Pool.Close()
}
