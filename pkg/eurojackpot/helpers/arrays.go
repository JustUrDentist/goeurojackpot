package helpers

import (
	"errors"
	"sort"
	"strconv"
)

func MapToIntArray(m map[int]int) []int {
	var result []int
	for _, n := range m {
		result = append(result, n)
	}
	return result
}

func IntArrayToSQLString(a []int) string {
	var result string

	lenArr := len(a)
	result += "'{"
	for i := 0; i < lenArr; i++ {
		result += strconv.Itoa(a[i])
		if i != lenArr-1 {
			result += ", "
		}
	}
	result += "}'"

	return result
}

func IntArrayToString(a *[]int) (string, error) {
	result := ""

	if len(*a) == 0 {
		err := errors.New("a has no values")
		return result, err
	}

	for i := 0; i < len(*a); i++ {
		if i != 0 {
			result += " "
		}
		result += strconv.Itoa((*a)[i])
	}

	return result, nil
}

func CompareIntArrays(a1 *[]int, a2 *[]int) bool {
	lenA1 := len(*a1)
	lenA2 := len(*a2)

	if lenA1 != lenA2 {
		return false
	}

	sort.Ints(*a1)
	sort.Ints(*a2)

	for i := 0; i < lenA1; i++ {
		if (*a1)[i] != (*a2)[i] {
			return false
		}
	}

	return true
}

func SumIntArray(a []int) int {
	var sum int
	for _, v := range a {
		sum += v
	}

	return sum
}
