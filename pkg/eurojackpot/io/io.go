package io

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

//Is looking for a file returns true if exist and false if not
func FileExists(fileName string) bool {
	info, err := os.Stat(fileName)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func Read(v interface{}, fileName string) {
	dat, err := ioutil.ReadFile("resources/" + fileName + ".json")
	check(err)
	err = json.Unmarshal(dat, v)
	check(err)
}

func Write(v interface{}, fileName string) {
	file, err := json.MarshalIndent(v, "", " ")
	check(err)
	err = ioutil.WriteFile("resources/"+fileName+".json", file, 0644)
	check(err)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
