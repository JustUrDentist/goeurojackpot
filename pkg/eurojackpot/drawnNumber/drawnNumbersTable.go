package drawnNumber

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/helpers"
	"time"
)

var (
	insertDrawnNumber         = "INSERT INTO winning_numbers (five_of_fifty, two_of_ten, date) VALUES "
	selectDrawnNumbers        = "SELECT five_of_fifty, two_of_ten, date FROM winning_numbers"
	selectLastDrawnNumberDate = "SELECT MAX(date) FROM winning_numbers"
)

func InsertDN(number DrawnNumber) {
	db := helpers.DatabaseHelper{}.New()

	query := insertDrawnNumber + db.Query(number.ToSQLString())

	db.Inserter(query)
}

func InsertDNs(numbers *[]DrawnNumber) {
	db := helpers.DatabaseHelper{}.New()

	query := insertDrawnNumber
	arrLen := len(*numbers)

	for i, n := range *numbers {
		query += db.Query(n.ToSQLString())

		if i < arrLen-1 {
			query += ", "
		}
	}

	db.Inserter(query)
}

func SelectLastDN() time.Time {
	db := helpers.DatabaseHelper{}.New()
	var result time.Time

	db.SingleSelect(selectLastDrawnNumberDate, &result)

	return result
}

func SelectDNs() []DrawnNumber {
	db := helpers.DatabaseHelper{}.New()
	var conn *pgxpool.Conn

	var rows pgx.Rows
	var result []DrawnNumber
	var fiveOfFifty, twoOfTen []int
	var date time.Time

	conn = db.AcquireConn()

	rows, db.Err = conn.Query(context.TODO(), selectDrawnNumbers)
	db.Catch()

	for rows.Next() {
		db.Err = rows.Scan(&fiveOfFifty, &twoOfTen, &date)
		db.Catch()

		winningNumber := DrawnNumber{
			Date: date.Format("1.2.2006"),
		}
		copy(winningNumber.FiveOfFifty[:], fiveOfFifty)
		copy(winningNumber.TwoOfTen[:], twoOfTen)

		result = append(result, winningNumber)
	}

	conn.Release()

	return result
}
