package drawnNumber

import (
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/helpers"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/io"
	"strconv"
)

type DrawnNumber struct {
	FiveOfFifty [5]int
	TwoOfTen    [2]int
	Date        string
}

func (d *DrawnNumber) ToSQLString() string {
	var date string

	date = d.Date[6:] + d.Date[2:6] + d.Date[0:2]

	return "(" + helpers.IntArrayToSQLString(d.FiveOfFifty[:]) + ", " + helpers.IntArrayToSQLString(d.TwoOfTen[:]) + ", '" + date + "')"
}

func StringToWinningNumber(s string) (DrawnNumber, error) {
	var result DrawnNumber
	var temp []string
	var err error

	i := 0
	temp = append(temp, "")
	for _, input := range s {
		if string(input) != " " {
			temp[i] += string(input)
		} else {
			i++
			temp = append(temp, "")
		}
	}

	for j := 0; j < len(temp)-1; j++ {
		if j < 5 {
			result.FiveOfFifty[j], err = strconv.Atoi(temp[j])
		} else {
			result.TwoOfTen[j-5], err = strconv.Atoi(temp[j])
		}

		if err != nil {
			return result, err
		}
	}

	result.Date = temp[len(temp)-1]

	return result, nil
}

type DrawnNumbers struct {
	Numbers []DrawnNumber
}

func (ds *DrawnNumbers) AddWinningNumber(newNumber DrawnNumber) {
	if ds.Numbers == nil {
		ds.Numbers = SelectDNs()
	}

	ds.Numbers = append(ds.Numbers, newNumber)

	io.Write(ds.Numbers, "eurojackpotNumbers")
}

func (ds *DrawnNumbers) AverageSum() int {
	sum := 0
	numLen := len(ds.Numbers)

	for i := 0; i < numLen; i++ {
		for j := 0; j < 5; j++ {
			sum += ds.Numbers[i].FiveOfFifty[j]
		}
	}

	if numLen == 0 {
		numLen = 1
	}

	return sum / numLen
}
