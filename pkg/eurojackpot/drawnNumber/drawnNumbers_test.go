package drawnNumber

import "testing"

func TestAverageSum(t *testing.T) {
	n1 := DrawnNumber{
		FiveOfFifty: [5]int{1, 2, 3, 4, 5},
		TwoOfTen:    [2]int{},
		Date:        "",
	}

	n2 := DrawnNumber{
		FiveOfFifty: [5]int{1, 2, 3, 4, 5},
		TwoOfTen:    [2]int{},
		Date:        "",
	}

	var ds DrawnNumbers

	ds.Numbers = append(ds.Numbers, n1)
	ds.Numbers = append(ds.Numbers, n2)

	avg := ds.AverageSum()

	if avg != 15 {
		t.Error("averageSum was incorrect. expected: ", 15, " get: ", avg)
	}
}
