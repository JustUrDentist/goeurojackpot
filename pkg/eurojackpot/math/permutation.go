package math

func Permutations(numbers []int, start int, end int, index int, combOf int, output *[][]int) {
	var temp []int
	allPermutations(numbers, &temp, start, end, index, combOf, output)
}

func allPermutations(numbers []int, temp *[]int, start int, end int, index int, combOf int, output *[][]int) {
	if len(*temp) == 0 {
		*temp = make([]int, combOf, combOf)
	}

	if index == combOf {
		var x = make([]int, combOf)
		copy(x, (*temp)[0:combOf])
		*output = append(*output, x)
		return
	}

	for i := start; i < end && end-i+1 > combOf-index; i++ {
		(*temp)[index] = numbers[i]
		allPermutations(numbers, temp, i+1, end, index+1, combOf, output)
	}
}
