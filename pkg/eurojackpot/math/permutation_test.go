package math

import "testing"

func TestPermutation(t *testing.T) {
	var output [][]int
	expected := [][]int{{1, 2}, {1, 3}, {2, 3}}

	Permutations([]int{1, 2, 3}, 0, 3, 0, 2, &output)

	if len(expected) != len(output) {
		t.Error("permutation was incorrect")
	}
	for i := 0; i < len(output); i++ {
		for j := 0; j < len(output[i]); j++ {
			if expected[i][j] != output[i][j] {
				t.Error("permutation was incorrect")
			}
		}
	}
}
