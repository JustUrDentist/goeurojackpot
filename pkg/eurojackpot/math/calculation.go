package math

func NextGreaterDivisor(number int, divisor int) int {
	if number < divisor {
		return 1
	}

	for number%divisor != 0 {
		divisor++
	}

	return divisor
}
