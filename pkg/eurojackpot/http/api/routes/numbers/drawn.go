package numbers

import (
	"encoding/json"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/combination"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/drawnNumber"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/http/api"
	"net/http"
)

func AllDrawnNumbers(w http.ResponseWriter, _ *http.Request) {

	api.ResponseWithJSON(w, http.StatusOK, drawnNumber.SelectDNs())
}

func AddDrawnNumber(w http.ResponseWriter, r *http.Request) {
	gen := combination.Generate{}
	var newNumber drawnNumber.DrawnNumber
	json.NewDecoder(r.Body).Decode(&newNumber)

	gen.DNs.AddWinningNumber(newNumber)

	api.ResponseWithJSON(w, http.StatusCreated, map[string]string{"message": "success"})
}
