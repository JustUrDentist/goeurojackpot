package numbers

import (
	"encoding/json"
	"fmt"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/combination"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/http/api"
	"net/http"
)

type parameterAllNumbers struct {
	CombinationOf int  `json:"combOf"`
	Count         int  `json:"count"`
	SumFrom       uint `json:"sumFrom"`
	SumTo         uint `json:"sumTo"`
	Limit         uint `json:limit`
	Page          uint `json:"page"`
}

func (p *parameterAllNumbers) isFilled() bool {
	return p.CombinationOf != 0 && p.Count != 0 && p.SumFrom >= 0 && p.SumTo > 14 && p.Limit != 0 && p.Page >= 0
}

type parameterUniqueNumbers struct {
	CombinationOf int  `json:"combOf"`
	Count         int  `json:"count"`
	SumFrom       uint `json:"sumFrom"`
	SumTo         uint `json:"sumTo"`
}

func (p *parameterUniqueNumbers) isFilled() bool {
	return p.CombinationOf != 0 && p.Count != 0 && p.SumFrom != 0 && p.SumTo > 14
}

func AllGeneratedNumbers(w http.ResponseWriter, r *http.Request) {
	var params parameterAllNumbers
	err := json.NewDecoder(r.Body).Decode(&params)
	if err != nil {
		api.ResponseWithError(w, http.StatusUnprocessableEntity, "missing parameter")
		return
	}

	if params.isFilled() {
		output := combination.SelectGeneratedCombinations(params.CombinationOf, params.Count, params.SumFrom, params.SumTo, params.Limit, params.Page)
		fmt.Println(params)
		api.ResponseWithJSON(w, http.StatusOK, output)
		return
	}
	api.ResponseWithError(w, http.StatusUnprocessableEntity, "missing parameter or wrong value")
}

func UniqueGeneratedNumbers(w http.ResponseWriter, r *http.Request) {
	var params parameterUniqueNumbers
	err := json.NewDecoder(r.Body).Decode(&params)
	if err != nil {
		api.ResponseWithError(w, http.StatusUnprocessableEntity, "missing parameter")
		return
	}

	if params.isFilled() {
		gen := combination.Generate{
			CombOf:    params.CombinationOf,
			CombCount: params.Count,
			SumFrom:   params.SumFrom,
			SumTo:     params.SumTo,
		}

		api.ResponseWithJSON(w, http.StatusOK, gen.RandomUniqueCombinations())
		return
	}
	api.ResponseWithError(w, http.StatusUnprocessableEntity, "missing parameter or wrong value")
}
