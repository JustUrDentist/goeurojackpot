package routes

import (
	"github.com/go-chi/chi"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/http/api/routes/calculation"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/http/api/routes/numbers"
)

func Routers() *chi.Mux {
	r := chi.NewRouter()

	r.Get("/numbers/drawn", numbers.AllDrawnNumbers)
	r.Post("/numbers/drawn", numbers.AddDrawnNumber)
	r.Get("/numbers/all", numbers.AllGeneratedNumbers)
	r.Get("/numbers/unique", numbers.UniqueGeneratedNumbers)
	r.Get("/calculation/update", calculation.Update)
	r.Get("/calculation/createNew", calculation.CreateAllNew)

	return r
}
