package calculation

import (
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/combination"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/http/api"
	"net/http"
)

func CreateAllNew(w http.ResponseWriter, _ *http.Request) {

	var gen combination.Generate

	go gen.AllNew()

	api.ResponseWithJSON(w, http.StatusOK, "calculation has started")
}
