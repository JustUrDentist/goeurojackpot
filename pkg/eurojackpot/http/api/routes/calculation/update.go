package calculation

import (
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/combination"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/http/api"
	"net/http"
)

func Update(w http.ResponseWriter, _ *http.Request) {

	if combination.UpdatedNeeded() {
		var gen combination.Generate

		go gen.Update()

		api.ResponseWithJSON(w, http.StatusOK, "Updated started")
	}

	api.ResponseWithJSON(w, http.StatusOK, "Update don't needed")
}
