package api

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func catch(err error) {
	if err != nil {
		panic(err)
	}
}

func ResponseWithError(w http.ResponseWriter, code int, msg string) {
	ResponseWithJSON(w, code, map[string]string{"error message": msg})
}

func ResponseWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, err := json.Marshal(payload)
	catch(err)
	fmt.Println(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	_, err = w.Write(response)
	catch(err)
}
