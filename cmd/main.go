package main

import (
	"bufio"
	"fmt"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/combination"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/drawnNumber"
	"os"
	"strconv"
)

func main() {
	gen := combination.Generate{
		CombOf:    2,
		CombCount: 2,
		GenNew:    false,
	}
	input := bufio.NewScanner(os.Stdin)

	fmt.Println("h: for Help")

	for {
		input.Scan()
		switch input.Text() {
		case "h":
			fmt.Println("\"new\" creates new random combinations with default parameters when not changed")
			fmt.Println("\"par\" change parameters")
			fmt.Println("\"add\" add a new Winning Number")
			fmt.Println("\"exit\" close the program")
		case "new":
			for _, value := range gen.RandomUniqueCombinations() {
				fmt.Println(value)
			}
		case "par":
			fmt.Println("\"comb\" for combinations change")
			fmt.Println("\"fre\" for frequency change")
			fmt.Println("\"gen\" for negating generate new")
			fmt.Println("\"back\" to go back")
			fmt.Println("combinations of: ", gen.CombOf)
			fmt.Println("frequency of: ", gen.CombCount)
			fmt.Println("generate new: ", gen.GenNew)
		parameterLoop:
			for {
				input.Scan()
				switch input.Text() {
				case "comb":
					fmt.Println("possible values 2 - 4")
					for {
						input.Scan()
						temp, _ := strconv.Atoi(input.Text())
						if temp >= 2 && temp <= 4 {
							gen.CombOf = temp
							break
						}
						fmt.Println("invalid value")
					}
				case "fre":
					fmt.Println("possible values ", gen.Combinations.MinCount, " - ", gen.Combinations.MaxCount)
					for {
						input.Scan()
						temp, _ := strconv.Atoi(input.Text())
						if temp >= gen.Combinations.MinCount && temp <= gen.Combinations.MaxCount {
							gen.CombCount = temp
							break
						}
					}
				case "gen":
					gen.GenNew = !gen.GenNew
				case "back":
					break parameterLoop
				}
			}
		case "add":
			fmt.Println("type as follows")
			fmt.Println("| 5 out of 50 | 2 of 10 | date |")
			fmt.Println("example: 5 10 17 25 30 2 8 25.09.2020")
			for {
				input.Scan()
				tempWN, err := drawnNumber.StringToWinningNumber(input.Text())
				if err != nil {
					fmt.Println("wrong input")
				} else {
					gen.DNs.AddWinningNumber(tempWN)
					break
				}
			}
		case "exit":
			return
		default:
			fmt.Println("h: for Help")
		}
	}

}
