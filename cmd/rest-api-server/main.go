package main

import (
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/helpers"
	"gitlab.com/JustUrDentist/GoEurojackpot/pkg/eurojackpot/http/api/routes"
	"net/http"
)

func main() {
	var db *helpers.DatabaseHelper
	db = helpers.DatabaseHelper{}.New()
	db.Open()
	defer db.Close()

	//wn := drawnNumber.DrawnNumber{
	//	FiveOfFifty: [5]int{15, 19, 34, 39, 49},
	//	TwoOfTen:    [2]int{2, 7},
	//	Date:        "02.10.2020",
	//}
	//
	//drawnNumber.InsertDN(wn)

	//gen := combination.Generate{
	//	CombOf:       0,
	//	CombCount:    0,
	//	DNs:           drawnNumber.DrawnNumbers{},
	//	Combinations: combination.Combinations{},
	//	RandomUnique: nil,
	//}

	//gen.Update()

	//gen.AllNew()

	r := routes.Routers()
	err := http.ListenAndServe(":8000", r)
	if err != nil {
		panic(err)
	}
}
